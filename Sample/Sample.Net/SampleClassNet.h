#pragma once
using namespace System::Runtime::InteropServices;
public ref class SampleClassNet
{
public:
	[UnmanagedFunctionPointer(CallingConvention::Cdecl)]
	delegate void SegmentCreatedDelegateNet(System::String^ arg);
	SampleClassNet(void);
	void DoWork(System::String^ directoryPath, SegmentCreatedDelegateNet^ segmentCreatedCallback, bool% cancel);
};