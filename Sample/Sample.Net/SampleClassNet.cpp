#include "stdafx.h"
#include "SampleClassNet.h"
#include "..\Sample\SampleClass.h"
//#include "SampleClass.h"
#include <iostream>

SampleClassNet::SampleClassNet(void)
{
}

void SampleClassNet::DoWork(System::String^ directoryPath, SegmentCreatedDelegateNet^ segmentCreatedCallback, bool% cancel)
{
	SampleClass* nativeClass = new SampleClass();

	System::IntPtr directoryPathPointer = System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(directoryPath);
	char *directoryPathNative = static_cast<char*>(directoryPathPointer.ToPointer());

	System::IntPtr callbackPointer = System::Runtime::InteropServices::Marshal::GetFunctionPointerForDelegate(segmentCreatedCallback);
	
	pin_ptr<bool> pinnedCancel = &cancel;
	bool* pinnedCancelRef = pinnedCancel;

	nativeClass->DoWork(directoryPathNative, (SegmentCreatedDelegate)(void*)callbackPointer, pinnedCancelRef);

	System::GC::KeepAlive(segmentCreatedCallback);
	System::Runtime::InteropServices::Marshal::FreeHGlobal(directoryPathPointer);

	delete nativeClass;
}