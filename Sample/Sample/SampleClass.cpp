#include "stdafx.h"
#include "SampleClass.h"
#include <string>
#include <ctime>
#include <iostream>

SampleClass::SampleClass(void)
{
}

SampleClass::~SampleClass(void)
{
	
}

void SampleClass::DoWork(char *directoryPath, SegmentCreatedDelegate callback, bool *cancel)
{
	clock_t beginingTime;
	char currentSegmentPath[256];
	int segmentIndex = 0;

	if(*cancel) return;

	char* segmentFileName = "test2%d.ts";

	FILE *pFilempegTs = NULL;
	beginingTime = clock();

	while(!*cancel)
	{
		// create the segment file to write if we haven't already.
		if(pFilempegTs == NULL)
		{
			segmentIndex++;
			strncpy(currentSegmentPath, directoryPath, sizeof(currentSegmentPath));
			strncat(currentSegmentPath, segmentFileName, sizeof(currentSegmentPath));
			sprintf(currentSegmentPath, currentSegmentPath, segmentIndex);
			if( (pFilempegTs  = fopen(currentSegmentPath, "wb" )) == NULL ) 
			{
				std::cout << "The file can not be opened for writing\n";
				return;
			}
		}

		if((double(clock() - beginingTime) / CLOCKS_PER_SEC) >= 2)
		{
			fclose(pFilempegTs);
			pFilempegTs = NULL;
			callback(currentSegmentPath);
			beginingTime = clock();
		}
	}  

	if(pFilempegTs != NULL)
	{
		fclose(pFilempegTs);
		pFilempegTs = NULL;
		callback(currentSegmentPath);
	}

	return;
}