#pragma once
typedef void (* SegmentCreatedDelegate)(char *arg);
class DllExport SampleClass
{
public:
	SampleClass(void);
	~SampleClass(void);
	void DoWork(char *directoryPath, SegmentCreatedDelegate callback, bool *cancel);
};