﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample.Cons
{
    class Program
    {
        static void Main(string[] args)
        {
            bool cancel = false;
            var sampleClass = new SampleClassNet();
            var directory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TestDir\\");
            if (Directory.Exists(directory))
                Directory.Delete(directory, true);
            Directory.CreateDirectory(directory);
            Task.Factory.StartNew(() =>
            {
                try
                {
                    sampleClass.DoWork(directory, (segment) => Console.WriteLine("Segment created: " + segment), ref cancel);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception: " + ex.Message);
                }
                GC.Collect();
            });
            Console.ReadLine();
            cancel = true;
        }
    }
}
